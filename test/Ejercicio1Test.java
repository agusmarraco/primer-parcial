import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Ejercicio1Test {

    @Test
    public void testSuccess() {
        String secuencia = "ACCGTACCGTACCTGACC";
        Ejercicio1 ejercicio1 = new Ejercicio1();
        String resultado = ejercicio1.secuenciaMasRepetida(secuencia, 3);
        Assertions.assertTrue("ACC".equals(resultado));
    }

    @Test
    public void testSuccessWithAMaxValueChange() {
        String secuencia = "ACCGTACCGTACCTGACCGTAGTA";
        Ejercicio1 ejercicio1 = new Ejercicio1();
        String resultado = ejercicio1.secuenciaMasRepetida(secuencia, 3);
        Assertions.assertTrue("GTA".equals(resultado));
    }

    @Test
    public void testSuccessWithDraw() {
        String secuencia = "ACCGTACCGTACCTGACCGTA";
        Ejercicio1 ejercicio1 = new Ejercicio1();
        String resultado = ejercicio1.secuenciaMasRepetida(secuencia, 3);
        Assertions.assertTrue("ACC".equals(resultado));
    }

    @Test
    public void testRuntimeException() {
        String secuencia = "ACCGTACCGTACCTGACCA";
        Ejercicio1 ejercicio1 = new Ejercicio1();
        Assertions.assertThrows(RuntimeException.class, () -> ejercicio1.secuenciaMasRepetida(secuencia, 3));
    }


    @Test
    public void testSuccessWithSize6() {
        String secuencia = "AAAAAAGGGGGG";
        Ejercicio1 ejercicio1 = new Ejercicio1();
        String resultado = ejercicio1.secuenciaMasRepetida(secuencia, 6);
        Assertions.assertTrue("AAAAAA".equals(resultado));
    }

}

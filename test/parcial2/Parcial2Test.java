package parcial2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Parcial2Test {

    private SalaDeJuegos salaDeJuegos;

    @BeforeEach
    public void setUp() {
        this.salaDeJuegos = new SalaDeJuegos();
    }

    @Test
    public void test() {
        JuegoTemplate juego = new JuegoLegajanos6(new Jugador("Adrian"), new Jugador("Andres"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(5, juego);

        juego = new JuegoLegajanos6(new Jugador("Adriana"), new Jugador("Ana"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(6, juego);

        juego = new JuegoElMenor(new Jugador("Carlos"), new Jugador("Cesar"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(3, juego);

        juego = new JuegoElMenor(new Jugador("Damian"), new Jugador("Demian"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(3, juego);

        juego = new JuegoLosImpares(new Jugador("Gustavo"), new Jugador("Gerardo"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(10, juego);

        juego = new JuegoLosImpares(new Jugador("Marian"), new Jugador("Maria"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(15, juego);

        juego = new JuegoLosImpares(new Jugador("Norma"), new Jugador("Nancy"));
        salaDeJuegos.agregarJuego(juego);
        hacerJugar(15, juego);

        Jugador jugador = salaDeJuegos.jugadorConMasDiamantes();
        System.out.println("Jugador con mas diamantes es " + jugador.getNombre() + " con " + jugador.getDiamantes() + " diamanates");

        jugador = salaDeJuegos.jugadorConMenosPuntos();
        System.out.println("Jugador con menos puntos es " + jugador.getNombre() + " con " + jugador.getPuntos() + " puntos");


    }


    private void hacerJugar(int veces, JuegoTemplate juego) {
        for (int i = 0; i < veces; i++) {
            juego.jugar();
        }
    }
}

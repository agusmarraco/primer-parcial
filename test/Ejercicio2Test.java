import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio2Test {


    @Test
    public void test2fijasExtremos() {
        String[] letras = {"A", "C", "T", "G"};
        String[] fijas = {"A", "0", "0", "G"};
        Ejercicio2 ejercicio2 = new Ejercicio2();
        List<String> combinaciones = new ArrayList<>();
        long init = System.currentTimeMillis();
        ejercicio2.combinaciones(letras, fijas, combinaciones);
        long end = System.currentTimeMillis();
        System.out.println("Combinaciones: " + combinaciones.size());
        Double time = Double.valueOf((end - init));
        Double seconds = time / 1000;
        System.out.println("Tardo: " + seconds.toString() + " segundos");
        System.out.println("Imprimo combinatorias");
        // combinaciones.forEach(x -> System.out.println(x));
        Assertions.assertTrue(combinaciones.size() == 16);
    }


    @Test
    public void test2MedioFijoSize3() {
        String[] letras = {"A", "C", "T"};
        String[] fijas = {"0", "C", "0"};
        Ejercicio2 ejercicio2 = new Ejercicio2();
        List<String> combinaciones = new ArrayList<>();
        long init = System.currentTimeMillis();
        ejercicio2.combinaciones(letras, fijas, combinaciones);
        long end = System.currentTimeMillis();
        System.out.println("Combinaciones: " + combinaciones.size());
        Double time = Double.valueOf((end - init));
        Double seconds = time / 1000;
        System.out.println("Tardo: " + seconds.toString() + " segundos");
        System.out.println("Imprimo combinatorias");
        Assertions.assertTrue(combinaciones.size() == 9);
        //combinaciones.forEach(x -> System.out.println(x));
    }

    @Test
    public void testPrimeroFijoSize4() {
        String[] letras = {"A", "C", "T", "G"};
        String[] fijas = {"A", "0", "0", "0"};
        Ejercicio2 ejercicio2 = new Ejercicio2();
        List<String> combinaciones = new ArrayList<>();
        long init = System.currentTimeMillis();
        ejercicio2.combinaciones(letras, fijas, combinaciones);
        long end = System.currentTimeMillis();
        System.out.println("Combinaciones: " + combinaciones.size());
        Double time = Double.valueOf((end - init));
        Double seconds = time / 1000;
        System.out.println("Tardo: " + seconds.toString() + " segundos");
        System.out.println("Imprimo combinatorias");
        Assertions.assertTrue(combinaciones.size() == 64);
        combinaciones.forEach(x -> System.out.println(x));
    }

    @Test
    public void test4782969Combinaciones() {
        String[] letras = {"A", "C", "T", "G", "B", "D", "E", "F", "G"};
        String[] fijas = {"A", "0", "0", "0", "B", "0", "0", "0", "0"};
        Ejercicio2 ejercicio2 = new Ejercicio2();
        List<String> combinaciones = new ArrayList<>();
        long init = System.currentTimeMillis();
        ejercicio2.combinaciones(letras, fijas, combinaciones);
        long end = System.currentTimeMillis();
        System.out.println("Combinaciones: " + combinaciones.size());
        Double time = Double.valueOf((end - init));
        Double seconds = time / 1000;
        System.out.println("Tardo: " + seconds.toString() + " segundos");
        System.out.println("Imprimo combinatorias");
        //combinaciones.forEach(x -> System.out.println(x));
    }
}

package parcial2;

import java.util.Random;

public class JuegoElMenor extends JuegoTemplate {

    public JuegoElMenor(Jugador jugador1, Jugador jugador2) {
        super(jugador1, jugador2);
    }

    @Override
    protected Integer generarPuntaje(Jugador jugador) {
        Random random = new Random();
        Integer puntaje = null;
        for (int i = 0; i < 10; i++) {
            Integer rand = random.nextInt(10) + 1;
            if (puntaje == null) {
                puntaje = rand;
            } else if (puntaje > rand) {
                puntaje = rand;
            }
        }
        return puntaje;
    }


}

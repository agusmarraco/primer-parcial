package parcial2;

import java.util.Random;

public class JuegoLegajanos6 extends JuegoTemplate {

    public JuegoLegajanos6(Jugador jugador1, Jugador jugador2) {
        super(jugador1, jugador2);
    }

    @Override
    protected Integer generarPuntaje(Jugador jugador) {
        Random random = new Random();
        Integer puntaje = 0;
        for (int i = 0; i < 10; i++) {
            Integer rand = random.nextInt(10);
            puntaje += Math.abs(rand - 6);
        }
        return puntaje;
    }


}

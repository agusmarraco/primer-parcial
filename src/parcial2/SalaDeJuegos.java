package parcial2;

import java.util.*;

public class SalaDeJuegos {

    private List<JuegoTemplate> juegos;

    public SalaDeJuegos() {
        this.juegos = new ArrayList<>();
    }

    public void agregarJuego(JuegoTemplate juego) {
        this.juegos.add(juego);
    }

    public Jugador jugadorConMasDiamantes() {
        Jugador elQueTieneMasDiamantes = null;
        Set<Jugador> jugadores = new HashSet<>();
        this.juegos.stream().forEach(jugador -> {
            jugadores.addAll(Arrays.asList(jugador.getJugador1(), jugador.getJugador2()));
        });
        for (Jugador jugador : jugadores) {
            if (elQueTieneMasDiamantes == null)
                elQueTieneMasDiamantes = jugador;
            if (elQueTieneMasDiamantes.getDiamantes() < jugador.getDiamantes())
                elQueTieneMasDiamantes = jugador;
        }
        return elQueTieneMasDiamantes;
    }

    public Jugador jugadorConMenosPuntos() {
        Jugador elQueTieneMenosPuntos = null;
        Set<Jugador> jugadores = new HashSet<>();
        this.juegos.stream().forEach(jugador -> {
            jugadores.addAll(Arrays.asList(jugador.getJugador1(), jugador.getJugador2()));
        });
        for (Jugador jugador : jugadores) {
            if (elQueTieneMenosPuntos == null)
                elQueTieneMenosPuntos = jugador;
            if (elQueTieneMenosPuntos.getPuntos() > jugador.getPuntos())
                elQueTieneMenosPuntos = jugador;
        }
        return elQueTieneMenosPuntos;
    }
}

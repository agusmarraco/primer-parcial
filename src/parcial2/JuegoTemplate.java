package parcial2;

public abstract class JuegoTemplate {

    private Jugador jugador1;

    private Jugador jugador2;

    public JuegoTemplate(Jugador jugador1, Jugador jugador2) {
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
    }

    public void jugar() {

        // calculo los puntos
        Integer puntaje1 = generarPuntaje(jugador1);
        jugador1.setPuntos(jugador1.getPuntos() + puntaje1);
        Integer puntaje2 = generarPuntaje(jugador2);
        jugador2.setPuntos(jugador2.getPuntos() + puntaje2);

        //calculo el ganador e imprimo
        calcularGanador(puntaje1, jugador1, puntaje2, jugador2);
    }

    protected abstract Integer generarPuntaje(Jugador jugador);


    private void calcularGanador(Integer puntajeJugador1, Jugador jugador1, Integer puntajeJugador2, Jugador jugador2) {
        Jugador ganador = null;
        Jugador perdedor = null;
        if (puntajeJugador1 > puntajeJugador2 || puntajeJugador1.equals(puntajeJugador2)) {
            ganador = jugador1;
            perdedor = jugador2;
        } else if (puntajeJugador1 < puntajeJugador2) {
            ganador = jugador2;
            perdedor = jugador1;
        }
        ganador.setDiamantes(ganador.getDiamantes() + 1);
        registrarGanador(ganador, perdedor);
    }

    private void registrarGanador(Jugador ganador, Jugador perdedor) {
        StringBuilder sb = new StringBuilder();
        sb = sb.append("Jugo ").append(ganador.getNombre()).append(" contra ").append(perdedor.getNombre())
                .append(".Gano ").append(ganador.getNombre()).append(" y la cantidad de diamantes que tiene ahora es ").append(ganador.getDiamantes());
        System.out.println(sb.toString());
    }

    public Jugador getJugador1() {
        return jugador1;
    }

    public void setJugador1(Jugador jugador1) {
        this.jugador1 = jugador1;
    }

    public Jugador getJugador2() {
        return jugador2;
    }

    public void setJugador2(Jugador jugador2) {
        this.jugador2 = jugador2;
    }
}

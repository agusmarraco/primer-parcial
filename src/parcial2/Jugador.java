package parcial2;

public class Jugador {

    private String nombre;

    private Integer diamantes;

    private Integer puntos;

    public Jugador(String nombre) {
        this.nombre = nombre;
        this.puntos = 0;
        this.diamantes = 0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDiamantes() {
        return diamantes;
    }

    public void setDiamantes(Integer diamantes) {
        this.diamantes = diamantes;
    }

    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }
}

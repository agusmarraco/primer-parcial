import java.util.Arrays;
import java.util.List;

public class Ejercicio2 {

    public void combinaciones(String[] letras, String[] fijas, List<String> combinaciones) {
        Integer size = letras.length;
        List<String> letrasList = Arrays.asList(letras);
        List<String> fijasList = Arrays.asList(fijas);
        for (String letra : letrasList) {
            Boolean esFija = esFija(letrasList, fijasList, size);
            String valorFijo = fijasList.get(letrasList.size() - size);
            if (esFija && letrasList.get(letrasList.size() - size).equals(letra)) {
                recursividad(size, valorFijo, combinaciones, letrasList, fijasList);
            } else if (!esFija) {
                recursividad(size, letra, combinaciones, letrasList, fijasList);
            }
        }
    }


    private void recursividad(Integer size, String value, List<String> resultados, List<String> letras, List<String> fijas) {
        if (size == 1) {
            resultados.add(value);
        } else {
            for (String letra : letras) {
                Boolean esFija = esFija(letras, fijas, (size - 1));
                String valorFijo = fijas.get(letras.size() - (size - 1));
                if (esFija && valorFijo.equals(letra)) {
                    recursividad(size - 1, value.concat(letra), resultados, letras, fijas);
                } else if (!esFija) {
                    recursividad(size - 1, value.concat(letra), resultados, letras, fijas);
                }

            }

        }
    }

    private Boolean esFija(List<String> letras, List<String> fijas, Integer size) {
        return !fijas.get(letras.size() - size).equals("0");
    }
}

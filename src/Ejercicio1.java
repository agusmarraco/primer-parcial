import java.util.*;

public class Ejercicio1 {

    private Map<String, Integer> counter;
    private Integer max;
    private List<String> maxKeys;
    private Map<String, Integer> letterValues = new HashMap<>();

    public String secuenciaMasRepetida(String secuenciaFuente, Integer size) {
        if (secuenciaFuente.length() % size != 0) {
            throw new RuntimeException("el size de la secuencia no es divisible por size pasado");
        }
        counter = new HashMap<>();
        loadLetterValues();
        int left = 0;
        int right = size;
        //conteo
        while (right <= secuenciaFuente.length()) {
            String subSequence = secuenciaFuente.substring(left, right);
            if (counter.containsKey(subSequence)) {
                Integer prevValue = counter.get(subSequence);
                prevValue += 1;
                counter.put(subSequence, prevValue);
            } else {
                counter.put(subSequence, 1);
            }
            left = right;
            right = left + size;
        }

        //maximo
        maxKeys = new ArrayList<>();
        for (String key : counter.keySet()) {
            if (max == null) {
                maxKeys.add(key);
                max = counter.get(key);
            } else if (max < counter.get(key)) {
                max = counter.get(key);
                maxKeys.clear();
                maxKeys.add(key);
            } else if (max.equals(counter.get(key))) {
                maxKeys.add(key);
            }
        }
        String finalKey = null;
        if (maxKeys.size() == 1) {
            finalKey = maxKeys.get(0);
        } else {
            Integer minAmount = null;
            String keyToReturn = null;
            for (String key : maxKeys) {
                if (minAmount == null) {
                    keyToReturn = key;
                    minAmount = calculatekeyValue(key);
                } else {
                    if (minAmount > calculatekeyValue(key)) {
                        minAmount = calculatekeyValue(key);
                        keyToReturn = key;
                    }
                }
            }
            finalKey = keyToReturn;
        }
        return finalKey;

    }


    private void loadLetterValues() {
        letterValues.put("A", 1);
        letterValues.put("C", 2);
        letterValues.put("G", 3);
        letterValues.put("T", 4);
    }

    private Integer calculatekeyValue(String key) {
        List<String> chars = Arrays.asList(key.split(""));
        Integer result = 1;
        for (String currentChar : chars) {
            result *= letterValues.get(currentChar);
        }
        return result;
    }
}
